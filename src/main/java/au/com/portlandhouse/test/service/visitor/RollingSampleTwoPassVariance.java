package au.com.portlandhouse.test.service.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class RollingSampleTwoPassVariance implements Visitor {
	
	private int windowSize;
	
	private List<Visitable> capmRelationship = new ArrayList<Visitable>();
	
	private Queue<Double> benchmarkReturns;

	public RollingSampleTwoPassVariance(int capacity) {
		this.windowSize = capacity;
		benchmarkReturns =  new ArrayBlockingQueue<Double>(windowSize);
	}
	
	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
		benchmarkReturns.add(element.getBenchmarkReturn());
		
		if (benchmarkReturns.size() == windowSize) {
			double meanBenchmarkReturn = element.getMeanBenchmarkReturn();

			double variance = 0.0d;
			
			ArrayList<Double> arrayBenchmarkReturns = new ArrayList<Double> (benchmarkReturns);

			for (int i = 0; i < arrayBenchmarkReturns.size(); i++) {
				variance += (arrayBenchmarkReturns.get(i) - meanBenchmarkReturn) * (arrayBenchmarkReturns.get(i) - meanBenchmarkReturn) / windowSize;
			} 
				
			element.setVariance(variance);
			
			benchmarkReturns.remove();
		}

		capmRelationship.add(element);
	}

	public List<Visitable> getCapmRelationship() {
		return capmRelationship;
	}

}
