package au.com.portlandhouse.test.service.visitor;

import java.util.ArrayList;
import java.util.List;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class RollingSampleBeta implements Visitor {
	
	private int windowSize;
	
	private List<Visitable> capmRelationship = new ArrayList<Visitable>();
	
	public RollingSampleBeta(int capacity) {
		this.windowSize = capacity;
	}
	
	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
		if (capmRelationship.size() >= windowSize) {
			double beta = element.getCovariance() / element.getVariance();
			element.setBeta(beta );
		}
		
		capmRelationship.add(element);
	}

	public List<Visitable> getCapmRelationship() {
		return capmRelationship;
	}

}
