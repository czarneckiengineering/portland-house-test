package au.com.portlandhouse.test.service;

import java.util.Random;

/**
 * The Class GaussianService.
 * 
 * Principle taken from <a href=
 * "http://www.javapractices.com/topic/TopicAction.do?Id=62">http://www.javapractices.com/topic/TopicAction.do?Id=62</a>
 * 
 * But remember variance is the square of standard deviation
 */
public class StatisticsService {

	private Random random = new Random();

	public double getGaussian(double mean, double standardDeviation) {
		double variance = standardDeviation * standardDeviation;
		return mean + random.nextGaussian() * variance;
	}

}
