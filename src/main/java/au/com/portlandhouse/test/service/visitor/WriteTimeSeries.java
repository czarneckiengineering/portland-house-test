package au.com.portlandhouse.test.service.visitor;

import java.text.SimpleDateFormat;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class WriteTimeSeries implements Visitor {
	
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	private StringBuilder sb = new StringBuilder();

	public void visit(TimeSeriesElement element) {
        sb.append(dateFormat.format(element.getTradingDate()));
        sb.append(",");
        sb.append(element.getBenchmarkReturn());
        sb.append(",");
        sb.append(element.getStrategyReturn());
        sb.append("\n");
	}

	public void visit(CapmSeriesElement element) {
        sb.append(dateFormat.format(element.getTradingDate()));
        sb.append(",");
        sb.append(element.getBenchmarkReturn());
        sb.append(",");
        sb.append(element.getStrategyReturn());
        sb.append("\n");
	}

	public String getText() {
		return sb.toString();
	}

}
