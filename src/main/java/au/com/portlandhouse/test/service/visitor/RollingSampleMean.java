package au.com.portlandhouse.test.service.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class RollingSampleMean implements Visitor {
	
	private int windowSize;
	
	private List<Visitable> capmRelationship = new ArrayList<Visitable>();
	
	private Queue<Double> benchmarkReturns;
	private double sumBenchmarkReturns = 0.0d;

	private Queue<Double> strategyReturns;
	private double sumStrategyReturns = 0.0d;
	
	public RollingSampleMean(int capacity) {
		this.windowSize = capacity;
		benchmarkReturns =  new ArrayBlockingQueue<Double>(windowSize);
		strategyReturns =  new ArrayBlockingQueue<Double>(windowSize);
	}
	
	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
		double benchmarkReturn = element.getBenchmarkReturn();
		double strategyReturn = element.getStrategyReturn();
		
		sumStrategyReturns += strategyReturn;
		sumBenchmarkReturns += benchmarkReturn;
		
		strategyReturns.add(strategyReturn);
		benchmarkReturns.add(benchmarkReturn);
		
		if (strategyReturns.size() == windowSize) {
			double meanStrategyReturn = sumStrategyReturns / windowSize;
			element.setMeanStrategyReturn(meanStrategyReturn);
			
			Double lastStrategyReturn = strategyReturns.remove();
			sumStrategyReturns -= lastStrategyReturn;
		}
		
		if (benchmarkReturns.size() == windowSize) {
			double meanBenchmarkReturn = sumBenchmarkReturns / windowSize;
			element.setMeanBenchmarkReturn(meanBenchmarkReturn);

			Double lastBenchmarkReturn = benchmarkReturns.remove();
			sumBenchmarkReturns -= lastBenchmarkReturn;
		}

		capmRelationship.add(element);
	}

	public List<Visitable> getCapmRelationship() {
		return capmRelationship;
	}

}
