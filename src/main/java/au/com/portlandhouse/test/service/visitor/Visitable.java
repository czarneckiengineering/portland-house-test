package au.com.portlandhouse.test.service.visitor;

public interface Visitable {
	
	void accept (Visitor visitor);

}
