package au.com.portlandhouse.test.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import au.com.portlandhouse.test.dao.HolidaysDao;
import au.com.portlandhouse.test.model.Market;

public class CalendarService {
	
	private HolidaysDao holidaysDao;
	
	public CalendarService(HolidaysDao holidaysDao) {
		this.holidaysDao = holidaysDao;
	}

	public void setHolidaysDao(HolidaysDao holidaysDao) {
		this.holidaysDao = holidaysDao;
	}

	public List<Date> getTradingDays(Market market, Date startDate, Date endDate) {
		List<Date> tradingDays = new ArrayList<Date> ();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		
		while (calendar.getTime().before(endDate)) {
			if(isTradingDay(market, calendar)) {
				tradingDays.add(calendar.getTime());
			}
			
			calendar.add(Calendar.DATE, 1);
		}
			
		return tradingDays;
	}

	public boolean isTradingDay(Market market, Calendar calendar) {
		boolean isTradingDay = true;
		
		// is date a weekend
		if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			isTradingDay = false;
		
		if (holidaysDao.isHoliday(market, calendar)) {
			isTradingDay = false;
		}
		
		return isTradingDay ;
	}
}
