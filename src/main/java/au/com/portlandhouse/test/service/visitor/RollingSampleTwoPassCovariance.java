package au.com.portlandhouse.test.service.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class RollingSampleTwoPassCovariance implements Visitor {
	
	private int windowSize;
	
	private List<Visitable> capmRelationship = new ArrayList<Visitable>();
	
	private Queue<Double> benchmarkReturns;

	private Queue<Double> strategyReturns;
	
	public RollingSampleTwoPassCovariance(int capacity) {
		this.windowSize = capacity;
		benchmarkReturns =  new ArrayBlockingQueue<Double>(windowSize);
		strategyReturns =  new ArrayBlockingQueue<Double>(windowSize);
	}
	
	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
		benchmarkReturns.add(element.getBenchmarkReturn());
		strategyReturns.add(element.getStrategyReturn());
		
		if (strategyReturns.size() == windowSize) {
			double meanStrategyReturn = element.getMeanStrategyReturn();
			double meanBenchmarkReturn = element.getMeanBenchmarkReturn();

			double covariance = 0.0d;
			
			ArrayList<Double> arrayStrategyReturns = new ArrayList<Double> (strategyReturns);
			ArrayList<Double> arrayBenchmarkReturns = new ArrayList<Double> (benchmarkReturns);

			for (int i = 0; i < arrayStrategyReturns.size(); i++) {
				covariance += (arrayStrategyReturns.get(i) - meanStrategyReturn) * (arrayBenchmarkReturns.get(i) - meanBenchmarkReturn) / windowSize;
			} 
				
			element.setCovariance(covariance);
			
			benchmarkReturns.remove();
			strategyReturns.remove();
		}

		capmRelationship.add(element);
	}

	public List<Visitable> getCapmRelationship() {
		return capmRelationship;
	}

}
