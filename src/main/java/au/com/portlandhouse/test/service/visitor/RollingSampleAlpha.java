package au.com.portlandhouse.test.service.visitor;

import java.util.ArrayList;
import java.util.List;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class RollingSampleAlpha implements Visitor {
	
	private int windowSize;
	
	private List<Visitable> capmRelationship = new ArrayList<Visitable>();
	
	public RollingSampleAlpha (int capacity) {
		this.windowSize = capacity;
	}
	
	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
		
		if (capmRelationship.size() >= windowSize) {
			double alpha = element.getMeanStrategyReturn() - element.getBeta() * element.getMeanBenchmarkReturn();
			element.setAlpha(alpha);
		}
			
		capmRelationship.add(element);
	}

	public List<Visitable> getCapmRelationship() {
		return capmRelationship;
	}

}
