package au.com.portlandhouse.test.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.portlandhouse.test.service.visitor.RollingSampleAlpha;
import au.com.portlandhouse.test.service.visitor.RollingSampleBeta;
import au.com.portlandhouse.test.service.visitor.RollingSampleMean;
import au.com.portlandhouse.test.service.visitor.RollingSampleTwoPassCovariance;
import au.com.portlandhouse.test.service.visitor.RollingSampleTwoPassVariance;
import au.com.portlandhouse.test.service.visitor.Visitable;

public class SeriesService {
	
	public List<Visitable> generateRelationship (List<Date> tradingDates, TradingStrategy tradingStrategy) {
		List<Visitable> relationship = new ArrayList<Visitable> ();
		for (Date tradingDate : tradingDates) {
			relationship.add(tradingStrategy.generateTimeSeriesElement(tradingDate));
		}
		
		return relationship;
	}

	public List<Visitable> populateMeans(List<Visitable> relationship, int windowSize) {
		RollingSampleMean visitor = new RollingSampleMean(windowSize);
		
		for (Visitable visitable : relationship) {
			visitable.accept(visitor);
		}
		
		return visitor.getCapmRelationship();
	}
	
	public List<Visitable> populateCovariance(List<Visitable> relationship, int windowSize) {
		RollingSampleTwoPassCovariance visitor = new RollingSampleTwoPassCovariance(windowSize);
		
		for (Visitable visitable : relationship) {
			visitable.accept(visitor);
		}
		
		return visitor.getCapmRelationship();
	}

	public List<Visitable> populateVariance(List<Visitable> relationship, int windowSize) {
		RollingSampleTwoPassVariance visitor = new RollingSampleTwoPassVariance(windowSize);
		
		for (Visitable visitable : relationship) {
			visitable.accept(visitor);
		}
		
		return visitor.getCapmRelationship();
	}

	public List<Visitable> populateBeta(List<Visitable> relationship, int windowSize) {
		RollingSampleBeta visitor = new RollingSampleBeta(windowSize);
		
		for (Visitable visitable : relationship) {
			visitable.accept(visitor);
		}
		
		return visitor.getCapmRelationship();
	}

	public List<Visitable> populateAlpha(List<Visitable> relationship, int windowSize) {
		RollingSampleAlpha visitor = new RollingSampleAlpha(windowSize);
		
		for (Visitable visitable : relationship) {
			visitable.accept(visitor);
		}
		
		return visitor.getCapmRelationship();
	}
	
}
