package au.com.portlandhouse.test.service;

import java.util.Date;

import au.com.portlandhouse.test.model.TimeSeriesElement;

public interface TradingStrategy {
	
	TimeSeriesElement generateTimeSeriesElement(Date tradingDate);

}
