package au.com.portlandhouse.test.service.visitor;

import java.text.SimpleDateFormat;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public class WriteCapmSeries implements Visitor {
	
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	private StringBuilder sb = new StringBuilder();

	public void visit(TimeSeriesElement element) {
	}

	public void visit(CapmSeriesElement element) {
        sb.append(dateFormat.format(element.getTradingDate()));
        sb.append(",");
        sb.append(element.getAlpha());
        sb.append(",");
        sb.append(element.getBeta());
        sb.append("\n");
	}

	public String getText() {
		return sb.toString();
	}

}
