package au.com.portlandhouse.test.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.service.visitor.Visitable;
import au.com.portlandhouse.test.service.visitor.WriteCapmSeries;
import au.com.portlandhouse.test.service.visitor.WriteTimeSeries;

public class CsvService {
	
	public void writeTimeSeriesFile (String filename, List<Visitable> list) throws IOException {
		
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));

        bw.write("Trading Date, Benchmark Return, Strategy Return");
		bw.newLine();
		
		WriteTimeSeries visitor = new WriteTimeSeries();
		for (Visitable visitable : list) {
			visitable.accept(visitor);
		}
		
		bw.write(visitor.getText());
		
        bw.flush();
        bw.close();
	}

	public List<Visitable> readTimeSeriesFile (String filename) throws IOException, ParseException {
		List<Visitable> relationship = new ArrayList<Visitable>();
		
		BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        boolean isHeader = true;
        
		while ((line = br.readLine()) != null) {
			if (isHeader) {
				isHeader = false;
				continue;
			}
            String[] fields = line.split(",");
            Visitable element = CapmSeriesElement.create(fields);
            relationship.add(element);
		}
		
		br.close();
		return relationship;
	}
	
	public void writeCapmSeriesFile (String filename, List<Visitable> list) throws IOException {
		
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));

        bw.write("Trading Date, Alpha, Beta");
		bw.newLine();
		
		WriteCapmSeries visitor = new WriteCapmSeries();
		for (Visitable visitable : list) {
			visitable.accept(visitor);
		}
		
		bw.write(visitor.getText());
		
        bw.flush();
        bw.close();
	}

}
