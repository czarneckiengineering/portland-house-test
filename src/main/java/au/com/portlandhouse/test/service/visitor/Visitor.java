package au.com.portlandhouse.test.service.visitor;

import au.com.portlandhouse.test.model.CapmSeriesElement;
import au.com.portlandhouse.test.model.TimeSeriesElement;

public interface Visitor {
	
	void visit(TimeSeriesElement element);

	void visit(CapmSeriesElement element);

}
