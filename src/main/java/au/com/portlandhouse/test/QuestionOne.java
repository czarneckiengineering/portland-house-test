package au.com.portlandhouse.test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import au.com.portlandhouse.test.dao.HolidaysDao;
import au.com.portlandhouse.test.model.Market;
import au.com.portlandhouse.test.model.TimeSeriesElement;
import au.com.portlandhouse.test.service.CsvService;
import au.com.portlandhouse.test.service.StatisticsService;
import au.com.portlandhouse.test.service.SeriesService;
import au.com.portlandhouse.test.service.CalendarService;
import au.com.portlandhouse.test.service.TradingStrategy;
import au.com.portlandhouse.test.service.visitor.Visitable;

public class QuestionOne {

	private static final double MEAN = 0.04;

	private static final double STANDARD_DEVIATION = 1.3;

	private HolidaysDao holidaysDao = new HolidaysDao();

	private CalendarService calendarService = new CalendarService(holidaysDao);

	private StatisticsService statisticsService = new StatisticsService();
	
	private CsvService csvService = new CsvService();
	
	private SeriesService seriesService = new SeriesService();

	public static void main(String[] args) throws IOException {
		QuestionOne questionOne = new QuestionOne();
		
		questionOne.writeTimeSeriesFiles();
	}

	void writeTimeSeriesFiles() throws IOException {
		Market market = Market.ASX;

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date endDate = calendar.getTime();
		
		calendar.add(Calendar.YEAR, -10);
		
		Date startDate = calendar.getTime();

		List<Date> tradingDays = calendarService.getTradingDays(market, startDate, endDate);
		
		writeTimeSeriesFile("strategy01.timeseries.csv", tradingDays, new TradingStrategy() {

			public TimeSeriesElement generateTimeSeriesElement(Date tradingDate) {
				double benchmark = statisticsService.getGaussian(MEAN, STANDARD_DEVIATION);
				double strategy = benchmark * 2;
				return new TimeSeriesElement().setTradingDate(tradingDate).setBenchmarkReturn(benchmark).setStrategyReturn(strategy);
			}
			
		});

		writeTimeSeriesFile("strategy02.timeseries.csv", tradingDays, new TradingStrategy() {

			public TimeSeriesElement generateTimeSeriesElement(Date tradingDate) {
				double benchmark = statisticsService.getGaussian(MEAN, STANDARD_DEVIATION);
				double strategy = statisticsService.getGaussian(MEAN, STANDARD_DEVIATION);
				return new TimeSeriesElement().setTradingDate(tradingDate).setBenchmarkReturn(benchmark).setStrategyReturn(strategy);
			}
			
		});

		writeTimeSeriesFile("strategy03.timeseries.csv", tradingDays, new TradingStrategy() {

			public TimeSeriesElement generateTimeSeriesElement(Date tradingDate) {
				double benchmark = statisticsService.getGaussian(MEAN, STANDARD_DEVIATION);
				double strategy = statisticsService.getGaussian(MEAN, STANDARD_DEVIATION) + benchmark * 0.5;
				return new TimeSeriesElement().setTradingDate(tradingDate).setBenchmarkReturn(benchmark).setStrategyReturn(strategy);
			}
			
		});

	}
	
	private void writeTimeSeriesFile(String filename, List<Date> tradingDays, TradingStrategy tradingStrategy) throws IOException {
		List<Visitable> relationship = seriesService.generateRelationship(tradingDays, tradingStrategy);
		csvService.writeTimeSeriesFile(filename, relationship);
	}

}
