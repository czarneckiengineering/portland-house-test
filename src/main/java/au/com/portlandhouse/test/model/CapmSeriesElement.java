package au.com.portlandhouse.test.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.portlandhouse.test.service.visitor.Visitable;
import au.com.portlandhouse.test.service.visitor.Visitor;

public class CapmSeriesElement  implements Visitable {
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public static CapmSeriesElement create(String[] fields) throws ParseException {
		CapmSeriesElement relationshipElement = new CapmSeriesElement();
		
		Date date = dateFormat.parse(fields[0]);
		relationshipElement.setTradingDate(date);
		double benchmark = Double.parseDouble(fields[1]);
		relationshipElement.setBenchmarkReturn(benchmark);
		double strategy = Double.parseDouble(fields[2]);
		relationshipElement.setStrategyReturn(strategy);
		
		return relationshipElement ;
	}

	private Date tradingDate;

	private double strategyReturn;

	private double benchmarkReturn;
	
	private double covariance;
	
	private double variance;

	private double beta;
	
	private double alpha;

	private double meanBenchmarkReturn;
	
	private double meanStrategyReturn;
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public double getAlpha() {
		return alpha;
	}

	public double getBenchmarkReturn() {
		return benchmarkReturn;
	}

	public double getBeta() {
		return beta;
	}

	public double getCovariance() {
		return covariance;
	}

	public double getVariance() {
		return variance;
	}

	public double getMeanBenchmarkReturn() {
		return meanBenchmarkReturn;
	}

	public double getMeanStrategyReturn() {
		return meanStrategyReturn;
	}

	public double getStrategyReturn() {
		return strategyReturn;
	}

	public Date getTradingDate() {
		return tradingDate;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public CapmSeriesElement setBenchmarkReturn(double benchmark) {
		this.benchmarkReturn = benchmark;
		return this;
	}

	public CapmSeriesElement setBeta(double beta) {
		this.beta = beta;
		return this;
	}

	public CapmSeriesElement setCovariance(double covariance) {
		this.covariance = covariance;
		return this;
	}

	public CapmSeriesElement setMeanBenchmarkReturn(double meanBenchmarkReturn) {
		this.meanBenchmarkReturn = meanBenchmarkReturn;
		return this;
	}

	public CapmSeriesElement setMeanStrategyReturn(double meanStrategyReturn) {
		this.meanStrategyReturn = meanStrategyReturn;
		return this;
	}

	public CapmSeriesElement setStrategyReturn(double strategy) {
		this.strategyReturn = strategy;
		return this;
	}

    public CapmSeriesElement setTradingDate(Date date) {
		this.tradingDate = date;
		return this;
	}

	public CapmSeriesElement setVariance(double variance) {
		this.variance = variance;
		return this;
	}

}
