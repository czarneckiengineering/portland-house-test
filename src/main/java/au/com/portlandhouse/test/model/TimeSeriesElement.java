package au.com.portlandhouse.test.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.portlandhouse.test.service.visitor.Visitable;
import au.com.portlandhouse.test.service.visitor.Visitor;

public class TimeSeriesElement implements Visitable {
	
	private Date tradingDate;
	
	private double benchmarkReturn;

	private double strategyReturn;

	public Date getTradingDate() {
		return tradingDate;
	}

	public TimeSeriesElement setTradingDate(Date date) {
		this.tradingDate = date;
		return this;
	}

	public double getBenchmarkReturn() {
		return benchmarkReturn;
	}

	public TimeSeriesElement setBenchmarkReturn(double benchmark) {
		this.benchmarkReturn = benchmark;
		return this;
	}

	public double getStrategyReturn() {
		return strategyReturn;
	}

	public TimeSeriesElement setStrategyReturn(double strategy) {
		this.strategyReturn = strategy;
		return this;
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public static TimeSeriesElement create(String[] fields) throws ParseException {
		TimeSeriesElement relationshipElement = new TimeSeriesElement();
		
		Date date = dateFormat.parse(fields[0]);
		relationshipElement.setTradingDate(date);
		double benchmark = Double.parseDouble(fields[1]);
		relationshipElement.setBenchmarkReturn(benchmark);
		double strategy = Double.parseDouble(fields[2]);
		relationshipElement.setStrategyReturn(strategy);
		
		return relationshipElement ;
	}

}
