package au.com.portlandhouse.test;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import au.com.portlandhouse.test.service.CsvService;
import au.com.portlandhouse.test.service.SeriesService;
import au.com.portlandhouse.test.service.visitor.Visitable;

public class QuestionTwo {

	private static final int WINDOW_SIZE = 120;

	private CsvService csvService = new CsvService();
	
	private SeriesService relationshipService = new SeriesService();
	
	public static void main(String[] args) throws IOException, ParseException {
		QuestionTwo questionTwo = new QuestionTwo();
		questionTwo.processTimeSeriesFiles();
	}

	void processTimeSeriesFiles() throws IOException, ParseException {
		processTimeSeriesFile ("strategy01.timeseries.csv", "strategy01.capm.csv");
		processTimeSeriesFile ("strategy02.timeseries.csv", "strategy02.capm.csv");
		processTimeSeriesFile ("strategy03.timeseries.csv", "strategy03.capm.csv");
	}

	private void processTimeSeriesFile(String timeSeries, String capmSeries) throws IOException, ParseException {
		List<Visitable> series = csvService.readTimeSeriesFile(timeSeries);
		
		series = relationshipService.populateMeans(series, WINDOW_SIZE);
		
		series = relationshipService.populateCovariance(series, WINDOW_SIZE);
		
		series = relationshipService.populateVariance(series, WINDOW_SIZE);
		
		series = relationshipService.populateBeta(series, WINDOW_SIZE);
		
		series = relationshipService.populateAlpha(series, WINDOW_SIZE);
		
		csvService.writeCapmSeriesFile(capmSeries, series);
	}

}
