package au.com.portlandhouse.test.dao.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "holidayList")
@XmlAccessorType (XmlAccessType.FIELD)
public class HolidayList {
	
    @XmlElement(name = "holiday")
	private List<Holiday> holidayList;

	public List<Holiday> getHolidayList() {
		return holidayList;
	}

	public void setHolidayList(List<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

}
