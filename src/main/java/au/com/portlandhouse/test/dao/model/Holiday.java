package au.com.portlandhouse.test.dao.model;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import au.com.portlandhouse.test.dao.DateAdapter;
import au.com.portlandhouse.test.model.Market;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "holiday")
@XmlAccessorType (XmlAccessType.FIELD)
public class Holiday {
	
	private Market market;
	
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date date;

	public Holiday() {
		super();
	}

	public Holiday(Market market, Date date) {
		super();
		this.market = market;
		this.date = date;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((market == null) ? 0 : market.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Holiday other = (Holiday) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (market != other.market)
			return false;
		return true;
	}

}
