package au.com.portlandhouse.test.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import au.com.portlandhouse.test.dao.model.Holiday;
import au.com.portlandhouse.test.dao.model.HolidayList;
import au.com.portlandhouse.test.model.Market;

public class HolidaysDao {
	
	private HolidayList holidays;
	
	{
		try {
			load();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isHoliday(Market market, Calendar calendar){
		boolean isHoliday = holidays.getHolidayList().contains(new Holiday(market, calendar.getTime()));
		
		return isHoliday;
	}

	public void load() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(HolidayList.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		File file = new File("holidays.xml");
		holidays = (HolidayList) jaxbUnmarshaller.unmarshal(file);
	}
}
