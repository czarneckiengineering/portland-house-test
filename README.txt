RELEASE NOTES
=============

1. The software source is delivered as a maven project, with project files to allow operation inside an Eclipse IDE.

2. The software can be built with the command 'mvn install'

3. The build executes two tests which will create six files in the project root directory:

     Generated time series data:
       strategy01.timeseries.csv
       strategy02.timeseries.csv
       strategy03.timeseries.csv

     CAPM analysis data:
       strategy01.capm.csv
       strategy02.capm.csv
       strategy03.capm.csv
       
4. The two main() entry points to the software are in classes QuestionOne and QuestionTwo

5. The principle model is in two classes TimeSeriesElement and CapmSeriesElement. All algorithms are formed of operations that 
   iterate over lists of these classes.

6. The controller logic is delivered in a set of *Service classes and in a Visitor pattern implementation

     CalendarService   
       - identifies weekdays and arbitrary trading holidays
     CsvService        
       - marshalls and unmarshalls CSV data from persistent stores
     SeriesService     
       - operations on the models (which are lists of TimeSeriesElement or CapmSeriesElement)
     StatisticsService 
       - just generates entries in a gaussian distribution with defined mean and standard deviation
     
     WriteTimeSeries   
       - serializes a single element to string
     WriteCapmSeries   
       - serializes a single CAPM element to string
       
     RollingSampleMean 
       - builds the strategy and benchmark mean values with a specified window size
     RollingSampleTwoPassCovariance
       - builds the covariance using the 'two pass covariance' algorithm
     RollingSampleTwoPassVariance
       - builds the variance using the 'two pass covariance' algorithm modified with just the benchmark values
     RollingSampleBeta
       - builds the beta using the values defined in the prior visitor classes
     RollingSampleAlpha
       - builds the alpha using the values defined in the prior visitor classes

7. There is also an exchange holiday implementation, implemented in HolidaysDao and reading XML data from holidays.xml

8. The software algorithm is build around the iteration across the model, which in all cases is a list of approx. 2600 value tuples.

9. The generation of the alpha and beta is broken down into separate calculations of each of the components defined in question 2, 
   starting with 'mean'
   
   An insight taken from the documentation [1] and [2] is that for 'online' algorithms, implementing a FIFO queue to hold 
   the contents of the 120 element window as well as a running total improves execution time.
   
   The calculation of variance leverages the knowledge from the implementation of 'mean' calculation, but has to perform an iteration 
   against the 120 elements of the 'window'. 
   
   I expected this double iteration to lead to a O(N^2) time complexity, but performance is quite good, so possibly O(N log N) is closer.
   
10. I did look for an 'online covariance with rolling window' calculation, but the closest was the 'online variance with rolling window' [2] 
    I have modified the critical pseudocode for the running totals below, but I am uncertain of the correctness of this code. 

11. I think, although it wasn't my intention, that separating the calculation of individual items in the defined function into visitors  
    has improved time performance.

12. I cannot judge the correctness of the files generated, but strategy01.capm.csv does have the expected 2.0 and 0.0 values (which was a relief)


References:
===========

[1] https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Covariance

[2] http://stackoverflow.com/questions/5147378/rolling-variance-algorithm

[3] http://rebcabin.github.io/blog/2013/01/22/covariance-matrices/

[4] https://en.wikipedia.org/wiki/Covariance

[5] https://onlinecourses.science.psu.edu/stat414/node/109


Working notes:
==============

The code for question one is quite neat - using Visitor pattern to explore the generated data sets and write CSV worked very well

Also have capability to add holiday calendars loaded as JAXB XML file

The code for the different trading strategies also factored out quite nicely into an anonymous class.



For question two, the calculation of covariance is the trick, and I have been reading around the various options.

A simple approach is in [1] but would tend to time order O(N ^ 2)

SO the intent must be to design an algorythm based on the Online covariance ... but with a rolling window

[2] has an implementation of rolling sample window for variance

My understanding is that the intent is to keep track of the addition to variance from the new observation MINUS the 
variance from throwing away the old observation (once we have 120 observations)

I could not find a rolling window covariance algorythm so my working assumption is that starting from the code in [2]
the calculation for 'accumulated covariance' is

accumulatedCovariance = 
                SQRT ((newBenchmarkReturn - benchmarkMean) * (newBenchmarkReturn - lastBenchmarkMean)
                    * (newStrategyReturn - strategyMean) * (newStrategyReturn - lastStrategyMean))
              - SQRT ((oldBenchmarkReturn - benchmarkMean) * (oldBenchmarkReturn - lastBenchmarkMean)
                    * (oldStrategyReturn - strategyMean) * (oldStrategyReturn - lastStrategyMean));
                    
                    
Need to think about this ... the time order tends to O (N log N) but extrapolating a new algorythm seems risky when the 'naive_covariance' algorythm in
[1] is so clear. 

So  it might be better to have all the variables for an online_covariance stored as fields in records in the 'CAPM enhanced relationship dataset'

i.e.

Loaded relationship record:

Relationship:
	Date: tradingDay
	double: referenceReturn
	double: strategyReturn
	

CAPM data enhanced relationship record:

CAPM_Relationship
	Date: tradingDay
	double: referenceReturn
	double: strategyReturn
	double: referenceMean
	double: strategyMean
	double accumulatedCovariance
	double: alpha
	double: beta
	
This way, the 'naive_covariance' algorythm could run just once over the data set, but save the various rolling values in each of the 120 'active' calculations.

But this is just working towards O(N^2) again!	

... why do you want to watch alpha and beta over time anyway; are we going to generate means of the calculated time series :-)




